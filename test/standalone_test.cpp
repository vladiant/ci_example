#include "lib.hpp"

#include <cstdlib>

int main() {
	for (std::size_t i = 0; i < 10; ++i) {
		if (lib::Add(i, i * 2) != i * 3) {
            return EXIT_FAILURE;
        }
	}
	return EXIT_SUCCESS;
}